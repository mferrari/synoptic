package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;

public class JMGroup extends JMComponent{
	private static final long serialVersionUID = 1L;
	private ArrayList <JMComponent> elements;
	//private ArrayList <JLineConnection> lines;
	private boolean showElement;
	
	
	public JMGroup(){
		elements=new ArrayList<JMComponent>();
		this.showLabel(false);
		showElement=true;
		copiable=false;
		
		this.setType("GROUP");
		this.setID("0000");
	}
	
	public void updateBounds(){

		int x=elements.get(0).getX();
		int y=elements.get(0).getY();
		for (JMComponent comp : elements){
			if (x>comp.getX())
				x=comp.getX();
			if (y>comp.getY())
				y=comp.getY();

		}
		int w=0,h=0;
		if (this.showElement){

			w=elements.get(0).getX()-x+elements.get(0).getWidth();
			h=elements.get(0).getY()-y+elements.get(0).getHeight();
			for (JMComponent comp : elements){
				if (w<comp.getX()-x+comp.getWidth())
					w=comp.getX()-x+comp.getWidth();
				if (h<comp.getY()-y+comp.getHeight())
					h=comp.getY()-y+comp.getHeight();

			}

			x-=getStep();
			y-=2*getStep();
			w+=2*getStep();
			h+=3*getStep();


		}
		else {
			h=3*getStep();
			w=type().length()*10+id().length()*10+getStep()*2;
		}
		

		this.realHeight=h;
		this.realWidth=w;
		
		setBounds(x,y,w,h);
		repaint();
		
		for (JLineConnection line:  getLines()){
			line.update();
		}	
		
	}
	
	public void addElement(JMComponent element){
		
		elements.add(element);
		add(element);
		element.showLabel(true);
		element.setEnabled(false);
		if (!this.showElement)
			this.setShowElement(true);
		else 
			updateBounds();
		//element.setBounds(element.getX()-getX(), element.getY()-getY(), element.getWidth(), element.getHeight());
	}
	
	public void addElements( ArrayList<JMComponent> list){
		
		for (JMComponent comp: list)
			addElement(comp);
		
	}
	
	public ArrayList<JMComponent> getElements() {
		return elements;
	}

	public boolean isShowElement() {
		return showElement;
	}


	public void setShowElement(boolean showElement) {
		
		this.showElement = showElement;
		updateBounds();
	}
	
	
	public void paint(Graphics g){
		
		Graphics2D g2 =(Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
		
		
		if (isSelected())
			super.drawSelectionBorder(g2);
		
		g2.setColor(new Color(72,62,55,180));
		g2.fillRoundRect(0,0, getWidth(), getHeight(),15,15);
		g2.setColor(new Color(186,186,186,200));
		g2.drawRoundRect(0,0, getWidth()-1, getHeight()-1,15,15);
		
		g2.setColor(new Color(172,165,155,255));
		g2.fillRoundRect(2,2, getWidth()-4, getStep(), 10, 10);
		g2.fillRect(2, 8,getWidth()-4, 10);
		
		g2.setColor(new Color(72,62,55,255));
		
		Font font= new Font("Nimbus Mono L",Font.BOLD,12);

		g2.setFont(font);
		
		g2.drawString(this.type()+":"+this.id(), 6, 14);

		
		g.translate(-getX(), -getY());
		if (showElement){
			super.paint(g);
		}
	}

	@Override
	public void setLocation(int x, int y){
		int dx,dy;
		dx=x-getX();
		dy=y-getY();
		super.setLocation(x, y);
		for (JMComponent element : elements)
			element.setLocation(element.getX()+dx,element.getY()+dy);
		
	}
}

