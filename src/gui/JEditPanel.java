package gui;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLayeredPane;


public class JEditPanel extends JLayeredPane{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public JEditPanel(){
		this.setLayout(null);
		this.setBackground(new Color(0,0,0,0));
	}
	
	public void paint(Graphics g){
		g.setColor(Color.lightGray);
		g.fillRect(0, 0, 70, getHeight());
		g.setColor(Color.darkGray);
		g.drawLine(70, 0, 70, getHeight());
		g.setColor(new Color(240,240,240));
		g.fillRect(71, 0, getWidth()-70, getHeight());
		g.setColor(new Color(255,255,255));
		
		for (int i=10;i<=getWidth()-70;i+=10){
			g.drawLine(70+i, 0, 70+i, getHeight());
		}
		for (int i=10;i<=getHeight();i+=10){
			g.drawLine(71, i, getWidth(), i);
		}
		
		super.paint(g);
		
	}
	
	public void update(Graphics g){
		paint(g);
	}
}
