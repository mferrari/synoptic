package gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.util.ArrayList;

import javax.swing.JComponent;

import core.SynObject;

public class JMComponent extends JComponent{
	
	public enum Action {
		NOTHING,DRAG,RESIZE,SELECT
	}

	private static final long serialVersionUID = 1L;
	
	private ArrayList<JMComponent> connections;
	private ArrayList<JLineConnection> lines;
	
	private int minWidth,minHeight;
	private int maxWidth,maxHeight;
	protected int realWidth,realHeight;
	private double hRatio;
	private double rotation=0.0;
	
	private int realX=-1,realY=-1;
	
	private int step;
	
	private boolean selected;
	private boolean dragged;
	protected boolean copiable;
	
	private boolean label;
	private boolean resizable;
	
	private Action courrentAction;
	
	private SynObject synObject;
	
//	protected String componentID;
//	protected String name;
	
	/*protected String name;
	protected String id;*/
	
	public JMComponent(){
		synObject=new SynObject();
		synObject.setType("component");
		synObject.setId("#0000");
		connections=new ArrayList<JMComponent>();
		lines=new ArrayList<JLineConnection>();
		copiable=false;
		selected=false;
		dragged=false;
		courrentAction=Action.NOTHING;
		minWidth=50;
		minHeight=50;
		maxWidth=150;
		maxHeight=150;
		hRatio=1.0;
		step=10;
		label=true;
		resizable=true;
	}
	
	public void setResizbale(boolean flg){
		resizable=flg;
	}
	
	public boolean isResizable(){
		return resizable;
	}
	
	public void showLabel(boolean flg){
		label=flg;
		this.setSize(realWidth, realHeight);
		this.repaint();
	}
	
	public boolean isLabelShown(){
		return label;
	}
	
	public ArrayList<JMComponent> getConnections() {
		return connections;
	}
	
	public ArrayList<JLineConnection> getLines(){
		return lines;
	}
	
	public void addLine(JLineConnection line){
		lines.add(line);
	}
	
	public void addConnection(JMComponent connection){
		connections.add(connection);
	}
	
	public boolean isConnected(JMComponent object){
		return connections.contains(object);
	}
	
	public boolean removeConnection(JMComponent object){
		return connections.remove(object);
	}
	
	public boolean removeLine(JLineConnection line){
		return lines.remove(line);
	}
	
	public Point getLeft(){
		return new Point(getX(),getY()+getRealHeight()/2);
	}
	
	public Point getRight(){
		return new Point(getX()+getRealWidth(),getY()+getRealHeight()/2);
	}


	public Point getUp(){
		return new Point(getX()+getRealWidth()/2,getY());
	}
	public Point getDown(){
		return new Point(getX()+getRealWidth()/2,getY()+getRealHeight());
	}
	
	public boolean isCopiable(){
		return copiable;
	}
	
	public void removeConnections(){
		for (int i=0;i<lines.size();i++){
			JMComponent left =lines.get(i).getLeft();
			JMComponent right=lines.get(i).getRight();
			if (left!=this && right==this){
				left.removeConnection(this);
				left.removeLine(lines.get(i));
			}
			else if (left==this && right!=this){
				right.removeConnection(this);
				right.removeLine(lines.get(i));
			}
			lines.get(i).setVisible(false);			
		}
		connections.clear();
	}
	
	@Override
	public void setLocation(int x,int y){
		if (realX==-1 || rotation==0.0){
			realX=x;
			realY=y;
		}
		else {
			realX+=x-getX();
			realY+=y-getY();
		}
		super.setLocation(x,y);
		for (JLineConnection line : lines){
			line.update();
		}
	
	}
	@Override
	public void setSize(int w,int h){
		if (w>=getMinWidth() && w<=getMaxHeight() && h>=getMinHeight() && h<=getMaxHeight()){
			int rh=(int)Math.round(w*this.gethRatio());			
			rh= step*(int)Math.round(rh/(double)step);
			int rw=step*(int)Math.round(w/(double)step);
			realWidth=rw;
			realHeight=rh;
			
			double angle=(isCopiable() ? 0.0 :rotation*Math.PI/180.0);
			double fw,fh;
	
			rh+=(isCopiable() || !this.label ? 0 : 27);
			
			fw=(double)rw*Math.cos(angle)-(double)rh*Math.cos(Math.PI/2.0+angle);
			fh=(double)rw*Math.sin(angle)+(double)rh*Math.sin(Math.PI/2.0+angle);
			
			double dx,dy;
			dx=(fw-rw)/2.0;
			dy=(fh-rh)/2.0;
			
			super.setBounds((int)Math.round(realX-dx),(int)Math.round(realY-dy),(int)Math.round(fw),(int)Math.round(fh));
			
			for (JLineConnection line:  getLines()){
				line.update();
			}	
		}
	}
	
	public JMComponent copy(){
		JMComponent copy=new JMComponent();
		copy.setBounds(getBounds());
		return copy;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
		this.repaint();
	}

	protected void drawSelectionBorder(Graphics g){
		if (!isEnabled()) return;
		Graphics2D g2=(Graphics2D)g;
		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
		if (!isDragged())
			g2.setColor(new Color(0,0,0,70));
		else if (getX()>70)
			g2.setColor(new Color(0,0,0,20));
		else 
			g2.setColor(new Color(255,0,0,70));
		g2.fillRoundRect(2, 2, getWidth()-4, getHeight()-4, 10, 10);
		g2.setColor(Color.DARK_GRAY);
		g2.setStroke(new BasicStroke(2));
		g2.drawRoundRect(3, 3, getWidth()-6, getHeight()-6, 10, 10);
		g2.setStroke(new BasicStroke(1));
	}

	protected void drawOverlay(Graphics g){
		Graphics2D g2=(Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_OFF);
		
		if (isSelected() && !isDragged() && isEnabled()){
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	                RenderingHints.VALUE_ANTIALIAS_ON);
			g2.setStroke(new BasicStroke(2));
			g2.setColor(Color.red);
			g2.drawArc(getWidth()-12, getHeight()-12, 10, 10,0,-90);
			g2.drawLine(getWidth()-12, getHeight()-2, getWidth()-7, getHeight()-2);
			g2.drawLine(getWidth()-2, getHeight()-12, getWidth()-2, getHeight()-7);

			//g2.drawRect(getWidth()-11, getHeight()-11, 10, 10);			
		}
		
	}
	
	protected void drawLabel(Graphics2D g2){
		if (!isCopiable() && label){
			g2.setStroke(new BasicStroke(1));
			g2.setColor(new Color(33,68,120,136));
			g2.fillRoundRect(3, getWidth(), getRealHeight()-6, 23, 5, 5);
			g2.setColor(new Color(0,0,0,170));
			g2.drawRoundRect(3, getWidth(), getRealHeight()-6, 23, 5, 5);
			Font font= new Font("Nimbus Mono L",Font.BOLD,11);
			g2.setFont(font);
			g2.setColor(Color.white);
			FontRenderContext frc= new  FontRenderContext(g2.getTransform(),true,false);
			int dx=(int)Math.round((getWidth()-font.getStringBounds(synObject.type(),frc).getWidth())/2.0);
			g2.drawString(synObject.type(), dx+1, getRealHeight()+10);
			dx=(int)Math.round((getWidth()-font.getStringBounds(synObject.id(),frc).getWidth())/2.0);
			g2.drawString(synObject.id(), dx+1, getRealHeight()+20);
		}
	}
	
	public void startAction(Action action){
		if (action==Action.NOTHING) return;
		else if (action==Action.DRAG){
			setDragged(true);
		}
		else if (action==Action.RESIZE){
			courrentAction=action;
		}
	}
	public void stopAction(){
		//setSize(getWidth(),getWidth());
		setDragged(false);
	}
	
	public boolean isDragged() {
		return dragged;
	}

	public void setDragged(boolean dragged) {
		this.dragged = dragged;
		if (dragged)
			courrentAction=Action.DRAG;
		else 
			courrentAction=Action.NOTHING;
	}
	
	public Action getCourrentAction(){
		return courrentAction;
	}
	
	
	public Action clicked(int x,int y){
		if (!isEnabled()) return Action.NOTHING;
		if (x>getWidth()-13 && y>getHeight()-13 && resizable) {
			return Action.RESIZE;
		}
		else{
			return Action.DRAG;
		}
	
	}

	public int getMinHeight() {
		return minHeight;
	}

	protected void setMinHeight(int minHeight) {
		this.minHeight = minHeight;
	}

	public int getMinWidth() {
		return minWidth;
	}

	protected void setMinWidth(int minWidth) {
		this.minWidth = minWidth;
	}

	public double gethRatio() {
		return hRatio;
	}

	protected void sethRatio(float hRatio) {
		this.hRatio = hRatio;
	}

	public int getMaxWidth() {
		return maxWidth;
	}

	protected void setMaxWidth(int maxWidth) {
		this.maxWidth = maxWidth;
	}

	public int getMaxHeight() {
		return maxHeight;
	}

	protected void setMaxHeight(int maxHeight) {
		this.maxHeight = maxHeight;
	}
	
	public int getStep(){
		return step;
	}

	public int getRealWidth() {
		return realWidth;
	}

	public int getRealHeight() {
		return realHeight;
	}

	public void setID(String id){
		synObject.setId(id);
	}
	public void setType(String type){
		synObject.setType(type);
		
	}
	public String id(){
		return synObject.id();
	}
	
	public String type(){
		return synObject.type();
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
		
		this.setSize(realWidth,realHeight);
	}
}
