package gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;

import javax.swing.JComponent;

import tools.Bezier;

public class JLine extends JComponent{

	private static final long serialVersionUID = 1L;
	private JMComponent first;
	
	private Point endPoint;
	private JLineConnection origin;
	private Bezier bezier;
	
	public JLine(JMComponent first){
		this.first=first;
		endPoint=new Point(first.getLocation());
		this.setBounds(first.getBounds());
		origin=null;
		bezier=new Bezier(first.getRight(),first.getRight(),false);
	}
	
	public JLine(JMComponent first, JLineConnection origin){
		this.first=first;
		endPoint=new Point(first.getLocation());
		this.setBounds(first.getBounds());
		this.origin=origin;
	}
	
	public void end(){
		first=null;
		endPoint=null;
	}
	
	public void update(Point p){
		this.setBounds((first.getX()<p.x ? first.getX() : p.x)-50, 
					   (first.getY()<p.y ? first.getY() : p.y)-50, 
					   Math.abs(first.getX()-p.x)+first.getWidth()*5, 
					   Math.abs(first.getY()-p.y)+first.getHeight()*5);
		
		endPoint = p;

	}
	
	public void paint(Graphics g){
		Graphics2D g2=(Graphics2D)g;
		g2.setColor(Color.black);
		g2.setStroke(new BasicStroke(2));
		

		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
		
		boolean vertical=false;
		int x0,y0,x1,y1;
		
		if (Math.abs(endPoint.x-first.getX())> first.getWidth()){
			x0=(endPoint.x>first.getX() ? first.getRight().x : first.getLeft().x)-getX();
			y0=first.getRight().y-getY();
			x1=endPoint.x-getX();
			y1=endPoint.y-getY();		
		}
		else {
			vertical=true;
			x0=first.getUp().x-getX();
			y0=(first.getY()<endPoint.y  ? first.getDown().y  : first.getUp().y)-getY();
			x1=endPoint.x-getX();
			y1=endPoint.y-getY();
			
		}
		
		bezier.update(new Point(x0,y0),new Point(x1,y1), vertical);	
		bezier.paint(g2);
		
		g2.drawArc(x1-6, y1-6, 12, 12, 0,360);


		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_OFF);
		
		g2.drawRect(x0-5, y0-5, 10, 10);
		
	}
	
	public JMComponent getFirst(){
		return first;
	}

	public JLineConnection getOrigin() {
		return origin;
	}

}
