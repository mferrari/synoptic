package gui;

import interfaces.LineMenuListener;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import events.LineEvent;

public class JLinePopup extends JPopupMenu{
	private static final long serialVersionUID = 1L;
    // Create the listener list
	protected javax.swing.event.EventListenerList listenerList =
		        new javax.swing.event.EventListenerList();
	private JLineConnection line;
	
	public JLinePopup(){
		initLineItems();
	}
	
	private void  initLineItems(){
		JMenuItem deleteItem=new JMenuItem("Delete");
		deleteItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				LineEvent evt=new LineEvent(line,this);
				for (LineMenuListener listener : listenerList.getListeners(LineMenuListener.class))
					listener.deleteEventOccurred(evt);
			}});
		add(deleteItem);
		
	}
	public void show(Component parent,JLineConnection line, int x, int y){
		super.show(parent,x,y);
		this.line=line;
	}
	
	

	public void addLineListener(LineMenuListener listener){
		listenerList.add(LineMenuListener.class, listener);
	}
	public void removeLineListener(LineMenuListener listener){
		listenerList.remove(LineMenuListener.class, listener);
	}
	
}
