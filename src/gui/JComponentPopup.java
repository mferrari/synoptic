package gui;

import interfaces.ComponentMenuListener;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import events.ComponentEvent;

public class JComponentPopup extends JPopupMenu{
	private static final long serialVersionUID = 1L;
    // Create the listener list
	protected javax.swing.event.EventListenerList listenerList =
		        new javax.swing.event.EventListenerList();

	private JMComponent component;

	public JComponentPopup(){
		initDeviceItems();
	}
	
	
	private void initDeviceItems(){
		JMenuItem deleteItem=new JMenuItem("Delete");
		deleteItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ComponentEvent evt=new ComponentEvent(component,this);
				for (ComponentMenuListener listener : listenerList.getListeners(ComponentMenuListener.class))
					listener.deleteEventOccurred(evt);
			}});
		add(deleteItem);
	
		
		JMenuItem editItem=new JMenuItem("Edit");
		editItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ComponentEvent evt=new ComponentEvent(component,this);
				for (ComponentMenuListener listener : listenerList.getListeners(ComponentMenuListener.class))
					listener.editEventOccurred(evt);
			}});
		add(editItem);
	}
	
	public void show(Component parent,JMComponent component, int x, int y){
		super.show(parent,x,y);
		this.component=component;
	}
	
	
	public void addComponentListener(ComponentMenuListener listener){
		listenerList.add(ComponentMenuListener.class,listener);
	}
	
	public void removeComponentListener(ComponentMenuListener listener){
		listenerList.remove(ComponentMenuListener.class, listener);
	}
	
}
