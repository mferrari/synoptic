package gui;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JComponent;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public class JContainer extends JComponent  implements MouseListener{
	private static final long serialVersionUID = 1L;
	protected JContainer parent=null;
	//List of objects
	protected Multimap<Class<? extends JMComponent>,JMComponent> devices;
	protected ArrayList<JLineConnection> lines;
	
	public JContainer(){
		initArray();
	}
	public JContainer(JContainer parent){
		this.parent=parent;
		initArray();
	}
	
	private void initArray(){
		devices=HashMultimap.create();
		lines=  new ArrayList<JLineConnection>();
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
