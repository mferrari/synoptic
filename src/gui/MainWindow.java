package gui;
import events.ComponentEvent;
import events.GroupEvent;
import events.LineEvent;
import gui.JLineConnection.LineAction;
import gui.JMComponent.Action;


import interfaces.ComponentMenuListener;
import interfaces.LineMenuListener;
import interfaces.GroupMenuListener;

import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;


import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import listeners.CListener;
import listeners.KListener;
import listeners.KPostListener;
import listeners.MMoveListener;



public class MainWindow extends JFrame implements MouseListener {
	//Swing UID (universal id)
	private static final long serialVersionUID = 1L;
	
	//List of objects
	private Multimap<Class<? extends JMComponent>,JMComponent> devices;
	private ArrayList<JLineConnection> lines;
	
	//Mouse movement listener
	private MMoveListener mMoveListener;
	private KPostListener kPostListener;
	private KListener kListener;
	private CListener cListener;
	
	//Object that identify the current dragged object
	private DragObject dragObject;
	
	private JMComponent lineEl0;
	
	private JSelector selector;
	
	//Root panel
	private JEditPanel editPanel;
	private JPanel leftPanel;
	//Layer id (constant)
	private static int objectLayer=0;
	private static int lineLayer=1;
	private static int selectLayer=2;
	private static int dragLayer=4;
	private static int shelfLayer=3;

	//Line tool selector
	private JToggleButton lineButton;


	private JLine line;
	
	//Constructor 
	public MainWindow(){
		//Initialise object
		dragObject =new DragObject();
		
		devices=HashMultimap.create();

		lines=  new ArrayList<JLineConnection>();
		
		mMoveListener=new MMoveListener(dragObject);
		kPostListener=new KPostListener(); 	
		kListener= 	  new KListener();
		cListener=    new CListener();
		
		this.setFocusable(true);
	
		
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
	    manager.addKeyEventPostProcessor(kPostListener);
	    
		
		
		//Disable mouse listener 
		this.addMouseListener(null);
		this.addMouseMotionListener(null);
		this.addKeyListener(kListener);
		
		lineEl0=null;

		
		//initialise GUI
		initGUI();
	
	}
	
	//Initialise GUI
	private void initGUI(){
		//setup window
		this.setBounds(100,100,1200,800);
		this.setTitle("Synoptic Creator");
		this.setLayout(null); //Using absolute layout

		//Setup root panel 
		editPanel=new JEditPanel();
		editPanel.setBounds(0, 0, getWidth(), getHeight()); 
		editPanel.addMouseListener(this); //setting (this) mouse listener 
		editPanel.addMouseMotionListener(mMoveListener); //external mouse movement listener
		editPanel.addComponentListener(cListener);
		editPanel.setBorder(new EmptyBorder(5, 5, 5, 5)); //no border
		editPanel.setLayout(null); // absolute design
		this.setContentPane(editPanel);
		
		//Creating shelf
		leftPanel= new JPanel();
		leftPanel.setBounds(0, 0,70,getHeight());
		leftPanel.setLayout(null);
		editPanel.add(leftPanel,shelfLayer,shelfLayer);
		cListener.addComponent(leftPanel);

		//Creating first elements 
		newDevice(10,10,true);
		newValve(10,80,true);
		
		//Initialise line tool selector
		lineButton= new JToggleButton("line");
		lineButton.setBounds(5, 150, 60, 25);
		lineButton.setFocusable(false);
		
		
		leftPanel.add(lineButton);
		//editPanel.add(lineButton,shelfLayer,shelfLayer);
		
	}
	
	private JDevice newDevice(int x,int y, boolean flag){
		JDevice dev=new JDevice(flag);
		
		
		if (!flag)
			editPanel.add(dev,objectLayer,devices.size());
		else
			editPanel.add(dev,shelfLayer,devices.size());
		
		dev.setLocation(x, y);
		dev.setSize(50,50);
		dev.setVisible(true);
		
		devices.put(JDevice.class,dev);
		
			
		dev.addMouseListener(this);
		dev.addMouseMotionListener(mMoveListener);
		return dev;
	}
	
	


	private void addComponent(JMComponent comp){
		devices.put(comp.getClass(),comp);
		editPanel.add(comp,objectLayer);
		
		comp.addMouseListener(this);
		comp.addMouseMotionListener(mMoveListener);
	}
	
	private JValve newValve(int x,int y,boolean flag){
		JValve valve=new JValve(flag);
		
		if (!flag)
			editPanel.add(valve,objectLayer,devices.size());
		else
			editPanel.add(valve,shelfLayer,devices.size());
		
		
		valve.setLocation(x, y);
		valve.setSize(50, 50);
		
		valve.setVisible(true);
		
		devices.put(JValve.class,valve);
		
		valve.addMouseListener(this);
		valve.addMouseMotionListener(mMoveListener);
		
		return valve;
	}
	
	private void removeFromPanel(ArrayList <JMComponent> list){
		for (JMComponent comp : list){
			editPanel.remove(comp);
			devices.remove(comp.getClass(),comp);
			comp.removeMouseListener(this);
			comp.removeMouseMotionListener(mMoveListener);
		}
		
	}
	
	private JMGroup newGroup(ArrayList <JMComponent> list){
		JMGroup group=new JMGroup();
		group.addElements(list);
		group.setVisible(true);
		
		editPanel.add(group,objectLayer,devices.size());
		
	
		removeFromPanel(list);
		
		devices.put(JMGroup.class,group);
		
		editPanel.repaint();
		group.repaint();

		group.addMouseListener(this);
		group.addMouseMotionListener(mMoveListener);
		
		return group;
	}
	
	private void delete(JMComponent comp){
		if (comp==null) return;
		comp.removeConnections();
		
		for (JLineConnection line : comp.getLines()) {
			lines.remove(line);
			editPanel.remove(line);
		}

		comp.getLines().clear();
		
		editPanel.remove(comp);
		devices.remove(comp.getClass(),comp);
		
		editPanel.repaint();
	}
	
	private void delete(JLineConnection line) {
		// TODO Auto-generated method stub
		line.destroy();
		editPanel.remove(line);
		lines.remove(line);
		editPanel.repaint();
	}
	
	
	private void setLayer(ArrayList<JMComponent> comps,int layer){
		for (JMComponent comp : comps)
			editPanel.setLayer(comp, layer,layer);
	}
	
	private JLineConnection newConnection(JMComponent lineEl1){
		if (lineButton.isSelected() && lineEl0!=null && lineEl0!=lineEl1 && !lineEl0.isConnected(lineEl1) && !lineEl1.isCopiable()) {
			JMComponent left=(lineEl0.getX()<lineEl1.getX() ? lineEl0 : lineEl1);
			JMComponent right=(lineEl0.getX()>=lineEl1.getX() ? lineEl0 : lineEl1);
			
			JLineConnection line=new JLineConnection(left,right);
			line.setVisible(true);
			
			line.addMouseListener(this);
			line.addMouseMotionListener(mMoveListener);
			
			lines.add(line);
			editPanel.add(line, lineLayer,lines.size());
			
			
			return line;
			
		}
		return null;	}
	
	
	
	public void deleteSelected(){
		for (JMComponent comp: getDeviceSelected()){
			delete(comp);
		}
		for (JLineConnection line : getLineSelected()){
			delete(line);
		}
		
	}

	

	private ArrayList<JLineConnection> getLineSelected() {	
		ArrayList<JLineConnection> lines=new ArrayList<JLineConnection>();
		for (JLineConnection line: this.lines){
			if (line.isSelected()) lines.add(line);
		}
		return lines;
	}
	
	public ArrayList<JMComponent> getDeviceSelected(){
		ArrayList<JMComponent> comps=new ArrayList<JMComponent>();
		for (JMComponent comp : devices.values()){
			if (comp.isSelected()) comps.add(comp);
		}
		return comps;
	}
	
	public void unselect(){
		for (JMComponent comp : getDeviceSelected()){
			comp.setSelected(false);
			comp.repaint();
		}
		for (JLineConnection line : getLineSelected()){
			line.setSelected(false);
			line.repaint();
		}
		
	}
	
	
	
	
	
	
	//Mouse Listener implementation
	@Override
	public void mouseClicked(MouseEvent e) {
		//POPUP
		
		if (e.getButton()==MouseEvent.BUTTON3){
			if (e.getComponent().getClass().getSuperclass().equals(JMComponent.class)){
				JMComponent source=(JMComponent)e.getComponent();
				
				if (source.isCopiable()) return;
				if (getDeviceSelected().size()>1 && getDeviceSelected().contains(source)) {
					JGroupPopup p=new JGroupPopup();
					p.show(e.getPoint(),getDeviceSelected(),e.getComponent());
					p.addGroupListener(new GroupMenuListener(){
						@Override
						public void createGroupEvent(GroupEvent evt) {
							newGroup(evt.getElements());
						}
						@Override
						public void deleteGroupEvent(GroupEvent evt) {
							deleteSelected();
						}});
					return;
				}
				else unselect();
				int x,y;
				x=e.getComponent().getX()+e.getX();
				y=e.getComponent().getY()+e.getY();
				JComponentPopup p=new JComponentPopup();
				source.setSelected(true);
								
				p.show(this,source,x,y);
				p.addComponentListener(new ComponentMenuListener(){
					@Override
					public void editEventOccurred(ComponentEvent evt) {
						// TODO Auto-generated method stub
						String oldID=evt.getComponent().id();
						String newID=(String)JOptionPane.showInputDialog(null,"Change ID","Edit Dialog",JOptionPane.PLAIN_MESSAGE,null,null,oldID);
						if (newID!=null && newID.length()>0){
							evt.getComponent().setID(newID);
							evt.getComponent().repaint();
						}
					}
					@Override
					public void deleteEventOccurred(ComponentEvent evt) {
						delete(evt.getComponent());
					}});
			}
			else if (e.getSource().getClass().equals(JLineConnection.class)){
				JLineConnection source=(JLineConnection)e.getSource();
				int x,y;
				x=e.getComponent().getX()+e.getX();
				y=e.getComponent().getY()+e.getY();
				
				if (getLineSelected().size()>1 && !getLineSelected().contains(source)) unselect();


				if (source.getAction(new Point(x,y))!=LineAction.nothing){
					JLinePopup p=new JLinePopup();
					source.setSelected(true);
					p.show(this,source,x,y);
					p.addLineListener(new LineMenuListener(){
						@Override
						public void deleteEventOccurred(LineEvent evt) {
							delete(evt.getLine());
						}});
				}
			
			}
		}
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		//NOTHING TO DO
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		//NOTHING TO DO
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getButton()==MouseEvent.BUTTON1 ) {
			int x,y;
			JComponent source=(JComponent)(e.getSource());
			if (source==null) return;
			
			x=source.getX()+e.getX();
			y=source.getY()+e.getY();
		
			
			if (source.getClass().equals(JLineConnection.class)){
				JLineConnection l=(JLineConnection)(source);
				if (l.getAction(new Point(x,y))==LineAction.edit){
					JMComponent comp=l.clicked(new Point(x,y));
			
					if (comp!=null){
						unselect();
						
						l.destroy();
						l.setVisible(false);
						
						lineEl0=comp;
						line=new JLine(comp,l);
						
						editPanel.add(line,lineLayer,lines.size());
						
						line.update(new Point(x,y));
						mMoveListener.setLine(line);
						
						return;
					}
				}
				else if (l.getAction(new Point(x,y))==LineAction.select){
					if (!kListener.isMultiSelected()) unselect();
					l.setSelected(true);
					return;
				}
			}
			
			for (JMComponent comp : devices.values()){
				if (comp.getBounds().contains(x, y)){
					if (comp.isCopiable()){
						unselect();
						JMComponent copy=comp.copy();
						copy.setSelected(true);
						addComponent(copy);
						dragObject.drag(copy,new Point(x,y));
						setLayer(dragObject.getObjects(), dragLayer);
						if (lineButton.isSelected()) lineButton.setSelected(false);
					}
					else if (x<70) return;
					else if (!lineButton.isSelected()) {
						if (!(kListener.isMultiSelected() || getDeviceSelected().contains(comp))) unselect();
						
						comp.setSelected(true);
						if (comp.clicked(x-comp.getX(), y-comp.getY())==Action.DRAG){
							dragObject.drag(getDeviceSelected(),new Point(x,y));
						}
						else if (comp.clicked(x-comp.getX(), y-comp.getY())==Action.RESIZE){
							dragObject.resize(comp);
						}
						setLayer(dragObject.getObjects(), dragLayer);
					}
					else {
						unselect();
						lineEl0=comp;
						line=new JLine(comp);
						editPanel.add(line,lineLayer,lines.size());
						line.update(new Point(comp.getX()+e.getX(),comp.getY()+e.getY()));
						mMoveListener.setLine(line);
					}
					return;
				}
				
			}
			unselect();
			selector=new JSelector(new Point(x,y));
			editPanel.add(selector,selectLayer,selectLayer);
			this.mMoveListener.setSelector(selector);
		}
		lineButton.setSelected(false);
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		int x,y;
		JComponent source=(JComponent)(e.getSource());
		if (source==null) return;
		
		x=source.getX()+e.getX();
		y=source.getY()+e.getY();
		
		if (e.getButton()==MouseEvent.BUTTON1){
			if (!dragObject.isStatus()){
				if ((line==null || lineEl0==null) && selector==null) return;
				if (selector==null){

					for (JMComponent dev : devices.values()){
				
						if (dev.getBounds().contains(x,y)){
							this.newConnection(dev);
						}
					}
				}
			}
			else if (dragObject.getX()<70) {
				deleteSelected();
				dragObject.drop();
			}
			else {
			
				double step=(double)dragObject.getObjects().get(0).getStep();
				dragObject.setLocation((int)step*(int)(Math.round(dragObject.getX()/step)),(int)step*(int)(Math.round(dragObject.getY()/step)));
				if (onGroup(dragObject.getRect())!=null && !dragObject.getObjects().contains(onGroup(dragObject.getRect()))){
					JMGroup g=onGroup(dragObject.getRect());
					g.addElements(dragObject.getObjects());
					
					removeFromPanel(dragObject.getObjects());
					
					dragObject.drop();
					
					g.repaint();
					editPanel.repaint();
					
				}
				else{
					setLayer(dragObject.getObjects(), objectLayer);
					dragObject.drop();
				}
			}
		}
		if (line!=null){
			
			editPanel.remove(line);
			mMoveListener.removeLine();
			if (line.getOrigin()!=null)	
				delete(line.getOrigin());
			line=null;	
			
			editPanel.repaint();
			
		}
		if (selector!=null){
			selectArea(selector.getBounds());
			editPanel.remove(selector);
			mMoveListener.removeSelector();
			selector=null;
			editPanel.repaint();
		}
	}
	
	
	private void selectArea(Rectangle area){
		for (JMComponent comp : devices.values()){
			if (!comp.isCopiable() && area.contains(comp.getBounds()))
				comp.setSelected(true);
		}
		for (JLineConnection line : lines){
			if (area.contains(line.getBounds()))
				line.setSelected(true);
		}
	
	}
//	
	private JMGroup onGroup(Rectangle r){
		JMGroup group=null;
		for (JMComponent g : devices.get(JMGroup.class)){
			if (g.getBounds().intersects(r)) return (JMGroup)g;
		}
		return group;
	}

	public void spaceAction() {
		for (JMComponent g : devices.get(JMGroup.class)){
			if (g.isSelected()){
				JMGroup g2= (JMGroup)g;
				g2.setShowElement(!g2.isShowElement());
			}
		}
		
		for (JMComponent c : devices.values()){
			if (c.isSelected() && !c.getClass().equals(JMGroup.class)){
				c.showLabel(!c.isLabelShown());
			}
		}
		
		
	}

}
