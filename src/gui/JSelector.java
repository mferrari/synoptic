package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JComponent;

public class JSelector extends  JComponent{
	private static final long serialVersionUID = 1L;
	private Point startPoint;
	
	public JSelector(Point startPoint){
		this.startPoint=startPoint;
	}

	public void updateGeometry(Point point){
		int x,y;
		int w,h;
		
		x=(point.x < startPoint.x ? point.x : startPoint.x );
		y=(point.y < startPoint.y ? point.y : startPoint.y );
		
		w=Math.abs(point.x-startPoint.x);
		h=Math.abs(point.y-startPoint.y);
		
		this.setBounds(x, y, w, h);
		this.repaint();
	}
	
	public void paint(Graphics g){
		g.setColor(new Color(128,229,255,90));
		g.fillRect(0, 0, getWidth(), getHeight());
		g.setColor(new Color(78,179,205,180));
		g.drawRect(0, 0, getWidth()-1, getHeight()-1);
	}

}
