package tools;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

public class Bezier {
	private Point p1,p2,p3,p4;
	private ArrayList<Point> curve;
	private boolean vertical=false;
	static private int size=20;
	
	public Bezier(Point p1,Point p4,boolean vertical){
		this.p1=p1;
		this.p4=p4;
		this.vertical=vertical;
		curve=new ArrayList<Point>();
		computeAnchors();
		computeCurve();
	}
	
	private void computeAnchors(){
		//size =(int)Math.round(Math.sqrt((double)((p4.x-p1.x)*(p4.x-p1.x)+(p4.y-p1.y)*(p4.y-p1.y)))/10.0);
		if (!vertical){
			int dx=p4.x-p1.x;
			p2=new Point(p1.x+dx/2,p1.y);
			p3=new Point(p4.x-dx/2,p4.y);
		}
		else {
			int dy=p4.y-p1.y;
			p2=new Point(p1.x,p1.y+dy/2);
			p3=new Point(p4.x,p4.y-dy/2);
		}
		
	}
	
	private void computeCurve(){
		double t=1.0/(double)(size-1.0);
		curve.clear();
		for (int i=0 ; i<size; i++){
			curve.add(computePoint(t*(double)i));
		}
		
	}
	
	private Point computePoint(double t){
		
		double rx,ry;    //resulting x,y 
		double ax,bx,cx; //x coefficients of the cubic spline
		double ay,by,cy; //y coefficients of the cubic spline
		double tS,tC; 	 //t Squared and t Cubed
		cx=3.0*(double)(p2.x-p1.x);
		bx=3.0*(double)(p3.x-p2.x)-cx;
		ax=(double)(p4.x-p1.x)-cx-bx;
		
		cy=3.0*(double)(p2.y-p1.y);
		by=3.0*(double)(p3.y-p2.y)-cy;
		ay=(double)(p4.y-p1.y)-cy-by;
		
		tS=(double)(t*t);
		tC=(double)(t*t*t);

		rx=(ax*tC)+(bx*tS)+(cx*t)+p1.x;
		ry=(ay*tC)+(by*tS)+(cy*t)+p1.y;
		
		return new Point((int)Math.round(rx),(int)Math.round(ry));
	}
	
	public void update(Point p1, Point p4,boolean vertical){
		this.p1=p1;
		this.p4=p4;
		this.vertical=vertical;
		computeAnchors();
	}
	
	
	
	public void paint(Graphics g){
		computeCurve();
		if (curve.size()<size)return;
		int x[],y[];
		x=new int[size];
		y=new int[size];
		for (int i=0; i<size;i++){
			x[i]=curve.get(i).x;
			y[i]=curve.get(i).y;
		}
		g.drawPolyline(x, y, size);
	}
	
	public boolean isClciked(Point p){
		if (curve.size()<size)return false;
		
		for (int i=0; i<size;i++){
			if (i<size-1) {
				if (Math.abs(curve.get(i).x-p.x)<Math.abs(curve.get(i).x-curve.get(i+1).x) && Math.abs(curve.get(i).y-p.y)<Math.abs(curve.get(i).y-curve.get(i+1).y)) 
					return true;
			}
		}

		return false;
	}
	
}
