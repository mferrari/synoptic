package interfaces;

import java.util.EventListener;

import events.ComponentEvent;

public interface ComponentMenuListener extends EventListener {
	public void editEventOccurred(ComponentEvent evt);
	public void deleteEventOccurred(ComponentEvent evt);
}
