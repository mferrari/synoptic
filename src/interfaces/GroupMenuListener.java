package interfaces;

import java.util.EventListener;

import events.GroupEvent;

public interface GroupMenuListener extends EventListener{
	public void createGroupEvent(GroupEvent evt);
	public void deleteGroupEvent(GroupEvent evt);
}
