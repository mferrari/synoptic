package events;

import gui.JMComponent;

import java.util.ArrayList;
import java.util.EventObject;

public class GroupEvent extends EventObject{
	private static final long serialVersionUID = 1L;

	private ArrayList<JMComponent> elements;
	
	public GroupEvent(ArrayList <JMComponent> elements,Object source) {
		super(source);
		this.elements=elements;
	}

	public ArrayList<JMComponent> getElements() {
		return elements;
	}
}
