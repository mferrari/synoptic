package listeners;


import gui.MainWindow;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KListener implements KeyListener {
	private boolean multiSelected;
	
	public KListener(){
		multiSelected=false;
	}
	
	public boolean isMultiSelected(){
		return multiSelected;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode()==KeyEvent.VK_CONTROL)
			multiSelected=true;
		
		
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode()==KeyEvent.VK_CONTROL)
			multiSelected=false;
		if (e.getKeyCode()==KeyEvent.VK_SPACE){
			MainWindow cast=(MainWindow)e.getSource();
    		cast.spaceAction();
		}
			
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
