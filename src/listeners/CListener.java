package listeners;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;

import javax.swing.JComponent;

public class CListener implements ComponentListener{
	private ArrayList<JComponent> components;
	
	public CListener(){
		components=new ArrayList<JComponent>();
	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentResized(ComponentEvent e) {
		// TODO Auto-generated method stub
		Component c = (Component)e.getSource();
	    
        // Get new size
        Dimension newSize = c.getSize();
        for (JComponent comp: components)
        	comp.setSize(comp.getWidth(), newSize.height);	
	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}
	public ArrayList<JComponent> getComponents() {
		return components;
	}
	public void addComponent(JComponent component) {
		this.components.add(component);
	}
	
	public void removeComponent(JComponent component){
		components.remove(component);
	}

}
