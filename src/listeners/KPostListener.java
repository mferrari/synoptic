package listeners;

import gui.MainWindow;

import java.awt.KeyEventPostProcessor;
import java.awt.event.KeyEvent;

public class KPostListener implements KeyEventPostProcessor {

    @Override
    public boolean postProcessKeyEvent(KeyEvent e) {
    	if (!e.getSource().getClass().equals(MainWindow.class)) return false;
    	
    	if (e.getKeyCode() == KeyEvent.VK_DELETE) {
    		MainWindow cast=(MainWindow)e.getSource();
    		cast.deleteSelected();
    		return true;    //halt further processing
        }
 
    	//if (e.getKeyCode()== KeyEvent.VK_UP){
    		//TODO move selected object with key.
    	//}
        return false;
    }

}
