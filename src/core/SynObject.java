package core;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.Document;

public class SynObject {
	private String type="NONE";
	private String id="0";
	
	private int x=0,y=0;
	private int width=0,height=0;
	
	private double tollerance=0.1,critical=0.2;

	
	private ArrayList<SynObject> connections;
	
	public SynObject(){
		connections=new ArrayList<SynObject>();
	}
	
	public void addConnection(SynObject obj){
		if (!connections.contains(obj))
			connections.add(obj);
	}
	
	public ArrayList<SynObject> getConnections(){
		return connections;
	}
	
	public void setPosition(int x,int y){
		this.y=y;
		this.x=x;
	}
	
	public void setSize(int w,int h){
		width=w;
		height=h;
	}
	
	
	public int x(){
		return x;
	}
	public int y(){
		return y;
	}
	public int width(){
		return width;
	}
	public int height(){
		return height;
	}
	
	public String type() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String id() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public Element getXMLElement(Document doc){
		Element element=doc.createElement("Object");
		element.setAttribute("type",type);// Or put the type like main tag??
		element.setAttribute("id",id);
		element.setAttribute("x",Integer.toString(x));
		element.setAttribute("y",Integer.toString(y));
		element.setAttribute("width",Integer.toString(width));
		element.setAttribute("height",Integer.toString(height));
		element.setAttribute("tollerance", Double.toString(tollerance));
		element.setAttribute("critical",Double.toString(critical));
		
		Element connection=doc.createElement("Connection");
		element.appendChild(connection);
		connection.setAttribute("size",Integer.toString( connections.size() ));
		
		String con="";
		for (SynObject obj : connections)
			con+=obj.type()+"@"+obj.id()+" ";
		
		connection.setAttribute("objects", con);
		
		return element;
	}
	
	
	
	
}
